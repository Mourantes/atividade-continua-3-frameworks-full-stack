import React from 'react';

export default function Home(){
    return (
        <div className='Home'>
            <h1 className='Boasvindas'>Bem vindo à Home da aplicação!</h1>
            <h4>O objetivo dessa AC é criar Criar uma aplicação frontend 
                em React e uma API no backend em Python. Esta é a aplicação
                em frontend. E o componente responsável é o chamado "API".</h4>
        </div>
    );
}
