import React, { useState, useEffect } from 'react';

export default function Api() {
    const [localuser, setLocaluser] = useState();

    const getApiLocal = async () => {
      const response = await fetch("http://127.0.0.1:5000/usuario", {
        method: 'GET',
        mode: 'no-cors',
        headers: {
            'Access-Control-Allow-Origin': '*',
            'Content-Type': 'application-json',
        }
        }
      ).then((response) => response.json());
      
      setLocaluser(response);
    };

    useEffect(() => {
      getApiLocal();
    }, [])

    return (
        <div className='app'>
            <p><strong>Usuários:</strong></p>
            {localuser && localuser.map((users) => (
                <div className='item-container'>
                    Id:{users.id}
                </div>
            ))}
        </div>
    );
}