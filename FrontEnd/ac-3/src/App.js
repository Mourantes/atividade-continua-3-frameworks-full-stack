import React from 'react';
import {Switch, Route, Link} from 'react-router-dom';
import Home from './Home';
import Api from './Api';
import "./App.css";


function App() {
  return (
    <>
    <header className='header'>
      <Link to='/home'>Home</Link>
      <Link to='/api'>API</Link>
    </header>
    <main>
      <Switch>
        <Route path='/home' component= {Home}/>
        <Route path='/api' component= {Api}/>
      </Switch>
    </main></>
  );
}

export default App;
