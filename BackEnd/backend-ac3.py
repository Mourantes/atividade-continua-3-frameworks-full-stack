from flask import Flask
import json

app = Flask(__name__)

tasks = [
        {
            "id": 1,
            "name": "Carlos Montenegro"
        },
        {   
            "id": 2,
            "name": "Renan Calheiros"
        },
        {   
            "id": 3,
            "name": "Henrico Galutto"
        },
        {   
            "id": 4,
            "name": "Francesca Djavan"
        },
        {   
            "id": 5,
            "name": "Francesca Djavan"
        }
    ]

tasksJSON = json.dumps(tasks)


@app.route('/')
def teste():
    return "App ativo"


@app.route('/usuario', methods=["GET"])
def tasks():
    return tasksJSON


if __name__ == '__main__':
    app.run()