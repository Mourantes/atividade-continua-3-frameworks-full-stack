# Atividade Contínua 3 - Frameworks Full Stack

## Motivo do Repositório
Este é um repositório para armazenar a Atividade Contínua 3 da matéria de Frameworks Fullsrack do curso de Sistemas da informação da Faculdade IMPACTA de Tecnologia. As aplicações estão em suas seguintes pastas.

### Objetivo:
Criar uma aplicação frontend em React e uma API no backend em Python. 
O Front tem que ter o componente que possa chamar uma API que estará em um Backend.
O Backend terá que ter uma API em GET exposta para que possa ser consumida.

## Informações dos Alunos
Nome: Murilo Rodrigues Moura - RA: 2102198 /
Nome: Gabriel Barbosa de Oliveira - RA:2102639 